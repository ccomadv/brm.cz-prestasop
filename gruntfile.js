module.exports = function(grunt) {
  grunt.initConfig({

  	// LESS Compilation
    less: {
      production: {
        options: {
          compress: false,
          yuicompress: false,
          optimization: 2
        },
        files: {
          // target.css file: source.less file
          'themes/brm/css/project.css' : 'themes/brm/less/project.less',
		  
        }
      }
    },

    // CSS concat
    concat_css: {
    	options: {},
    	all: {
    		src: [
    		'themes/brm/css/project.css'
			
			
    		
          ],
    		dest: 'themes/brm/css/full-project.css'
    	}
    },
	

    // CSS minification
    cssmin: {
      minify: {
        src: ['themes/brm/css/full-project.css'],
        dest: 'themes/brm/css/full-project.min.css'
      }
    },





    // watcher
    watch: {
      styles: {
        files: ['themes/brm/less/**/*.less'], // which files to watch
        tasks: ['less', 'concat_css', 'cssmin'],
        options: {
          nospawn: true
        }
      },
	  
	  
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-concat-css');
  
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);
  grunt.registerTask('styles', ['less', 'concat_css', 'cssmin']);

  grunt.registerTask('production', ['less', 'concat_css', 'cssmin']);
};