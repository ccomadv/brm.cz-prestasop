<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockfacebook}brm>blockfacebook_43d541d80b37ddb75cde3906b1ded452'] = 'Blok Facebook like';
$_MODULE['<{blockfacebook}brm>blockfacebook_e2887a32ddafab9926516d8cb29aab76'] = 'Zobrazí blok pro přidání vašich fanoušků na facebook stránku.';
$_MODULE['<{blockfacebook}brm>blockfacebook_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Konfigurace byla aktualizována';
$_MODULE['<{blockfacebook}brm>blockfacebook_f4f70727dc34561dfde1a3c529b6205c'] = 'Nastavení';
$_MODULE['<{blockfacebook}brm>blockfacebook_c98cf18081245e342d7929c117066f0b'] = 'Facebook odkaz (URL)';
$_MODULE['<{blockfacebook}brm>blockfacebook_c9cc8cce247e49bae79f15173ce97354'] = 'Uložit';
$_MODULE['<{blockfacebook}brm>blockfacebook_374fe11018588d3d27e630b2dffb7909'] = 'Nasledujte nás na Facebooku';
$_MODULE['<{blockfacebook}brm>preview_374fe11018588d3d27e630b2dffb7909'] = 'Nasledujte nás na Facebooku';
