<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_988659f6c5d3210a3f085ecfecccf5d3'] = 'Vlastní CMS informační blok';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_cd4abd29bdc076fb8fabef674039cd6e'] = 'Přidejte vlastní informační blok na vaše stránky.';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_d52eaeff31af37a4a7e0550008aff5df'] = 'Došlo k chybě během uložení.';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_6f16c729fadd8aa164c6c47853983dd2'] = 'Nový CMS blok';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'Popis';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_c9cc8cce247e49bae79f15173ce97354'] = 'Uložit';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_630f6dc397fe74e52d5189e2c80f282b'] = 'Zpět na výpis';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_6fcdef6ca2bb47a0cf61cd41ccf274f4'] = 'Blok ID';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_56425383198d22fc8bb296bcca26cecf'] = 'Text bloku';
$_MODULE['<{blockcmsinfo}brm>blockcmsinfo_ef61fb324d729c341ea8ab9901e23566'] = 'Přidat nový';
