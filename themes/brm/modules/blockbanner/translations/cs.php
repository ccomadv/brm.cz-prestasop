<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbanner}brm>blockbanner_4b92fcfe6f0ec26909935aa960b7b81f'] = 'Blok Banner';
$_MODULE['<{blockbanner}brm>blockbanner_df7859ac16e724c9b1fba0a364503d72'] = 'Vyskytla se chyba při nahrávání obrázku.';
$_MODULE['<{blockbanner}brm>blockbanner_efc226b17e0532afff43be870bff0de7'] = 'Nastavení bylo aktualizováno.';
$_MODULE['<{blockbanner}brm>blockbanner_f4f70727dc34561dfde1a3c529b6205c'] = 'Nastavení';
$_MODULE['<{blockbanner}brm>blockbanner_112f6f9a1026d85f440e5ca68d8e2ec5'] = 'Prosíme, vložte krátký, ale výstižný popisek k tomuto banneru.';
$_MODULE['<{blockbanner}brm>blockbanner_c9cc8cce247e49bae79f15173ce97354'] = 'Uložit';
